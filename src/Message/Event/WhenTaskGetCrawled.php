<?php

declare(strict_types=1);

namespace App\Message\Event;

final class WhenTaskGetCrawled
{
    private string $code;
    private array $data;

    public function __construct(string $code, array $data)
    {
        $this->code = $code;
        $this->data = $data;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getData(): array
    {
        return $this->data;
    }
}
