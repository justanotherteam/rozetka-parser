<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Message\Event\WhenTaskGetCrawled;
use App\Message\TaskMessage;
use App\Service\ParserInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class TaskMessageHandler implements MessageHandlerInterface
{
    private ParserInterface $parser;
    private MessageBusInterface $bus;

    public function __construct(ParserInterface $parser, MessageBusInterface $bus)
    {
        $this->parser = $parser;
        $this->bus = $bus;
    }

    public function __invoke(TaskMessage $taskMessage)
    {
        $data = $this->parser->parse($taskMessage->getCode());
        $event = new WhenTaskGetCrawled($taskMessage->getCode(), $data);
        $this->bus->dispatch(
            new Envelope($event))->with(new DispatchAfterCurrentBusStamp()
        );
    }
}
