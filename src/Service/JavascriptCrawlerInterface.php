<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Panther\Client;

interface JavascriptCrawlerInterface
{
    public function getClient(): Client;
}
