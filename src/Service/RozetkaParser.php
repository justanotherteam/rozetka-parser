<?php

declare(strict_types=1);

namespace App\Service;

use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\TimeoutException;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

final class RozetkaParser implements ParserInterface
{
    private const CSS_SELECTORS = [
        'price' => '.product-prices__big',
        'oldPrice' => '.product-prices__small',
        'characteristicsKey' => '.characteristics-full__label',
        'characteristicsValue' => '.characteristics-full__value',
        'image' => '.product-photo__picture',
        'searchLink' => '.goods-tile__heading',
        'searchHeading' => '.search-header',
    ];
    private const EXCLUDE_CHARACTERISTICS = [
        'Признаки легальности алкоголя',
    ];
    private string $url;
    private JavascriptCrawlerInterface $crawler;

    public function __construct(string $url, JavascriptCrawlerInterface $crawler)
    {
        $this->url = $url;
        $this->crawler = $crawler;
    }

    /**
     * @throws NoSuchElementException
     * @throws TimeoutException
     */
    public function parse(string $code): array
    {
        $client = $this->crawler->getClient();
        $responsePage = $client->request('GET', sprintf('%s/search/?text=%s', $this->url, $code));

        if ($responsePage->filter(self::CSS_SELECTORS['searchHeading'])->count()) {
            $client->click($client->getCrawler()->filter(self::CSS_SELECTORS['searchLink'])->first()->link());
        }
        $page = $client->waitFor(self::CSS_SELECTORS['price']);
        $price = $page->filter(self::CSS_SELECTORS['price'])->text();
        $imageUrl = $page->filter(self::CSS_SELECTORS['image'])->getAttribute('src');
        $oldPrice = $page->filter(self::CSS_SELECTORS['oldPrice']);
        $title = $page->filter('.product__title')->text();
        $url = $client->getCurrentURL();
        $discount = false;

        if ($oldPrice->count()) {
            $discount = true;
            $oldPriceText = $oldPrice->text();
            $oldPrice = mb_substr($oldPriceText, 0, mb_strlen($oldPriceText) - 1);
        }
        $characteristicsPage = $client->clickLink('Характеристики');
        $keys = $characteristicsPage
            ->filter(self::CSS_SELECTORS['characteristicsKey'])
            ->each(function (Crawler $key): string {
                return $key->text();
            });
        $values = $characteristicsPage
            ->filter(self::CSS_SELECTORS['characteristicsValue'])
            ->each(function (Crawler $value): string {
                return $value->text();
            });
        $characteristics = array_combine($keys, $values);
        $characteristics = $this->excludeData($characteristics);
        $comments = [];
        $page = $client->waitFor('.tabs__item > .tabs__link');

        if ($page->filter('.tabs__item > .tabs__link')->count()) {
            $commentLink = $page->filter('.tabs__item > .tabs__link')->first()->link();
            $commentBlocks = $client->click($commentLink)->filter('.comment__inner');
            $comments = $commentBlocks->each(function (Crawler $comment): array {
                $date = $comment->filter('.comment__date')->text();
                $author = str_replace($date, '', $comment->filter('.comment__author')->text());
                $text = $comment->filter('.comment__body')->text();
                $stars = $comment->filter('.rating-stars')->filter('stop[stop-color="#d2d2d2"][offset="100%"]')->count();

                return [
                    'date' => $date,
                    'author' => $author,
                    'text' => $text,
                    'stars' => $stars,
                ];
            });
        }
        $data = [
            'url' => $url,
            'title' => $title,
            'code' => $code,
            'price' => mb_substr($price, 0, mb_strlen($price) - 1),
            'characteristics' => $characteristics,
            'imageUrl' => $imageUrl,
            'isDiscount' => $discount,
            'comments' => $comments,
        ];

        if ($discount) {
            $data = $data + ['oldPrice' => $oldPrice];
        }

        if (!$data) {
            throw new ServiceUnavailableHttpException();
        }
        $client->quit();

        return $data;
    }

    private function excludeData(array $data): array
    {
        foreach (self::EXCLUDE_CHARACTERISTICS as $key) {
            unset($data[$key]);
        }

        return $data;
    }
}
