<?php

declare(strict_types=1);

namespace App\Service;

use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\TimeoutException;

interface ParserInterface
{
    /**
     * @throws NoSuchElementException
     * @throws TimeoutException
     */
    public function parse(string $code): array;
}
