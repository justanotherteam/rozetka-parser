<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Panther\Client;

final class JavascriptCrawlerChrome implements JavascriptCrawlerInterface
{
    private string $driverUrl;

    public function __construct(string $driverUrl)
    {
        $this->driverUrl = $driverUrl;
    }

    public function getClient(): Client
    {
        $port = rand(9515, 9815);

        return Client::createChromeClient($this->driverUrl, ['--no-sandbox', '--headless', '--disable-dev-shm-usage', '--disable-gpu'], ['port' => $port]);
    }
}
