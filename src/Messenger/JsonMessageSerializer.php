<?php

declare(strict_types=1);

namespace App\Messenger;

use App\Message\Event\WhenTaskGetCrawled;
use App\Message\TaskMessage;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;

final class JsonMessageSerializer implements SerializerInterface
{
    public function decode(array $encodedEnvelope): Envelope
    {
        $code = $encodedEnvelope['body'];
        $message = new TaskMessage($code);
        $stamps = [];

        return new Envelope($message, $stamps);
    }

    public function encode(Envelope $envelope): array
    {
        /** @var WhenTaskGetCrawled $message */
        $message = $envelope->getMessage();

        return [
            'body' => json_encode([
                'code' => $message->getCode(),
                'data' => $message->getData(),
            ]),
        ];
    }
}
