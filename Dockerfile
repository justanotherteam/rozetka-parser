FROM enable934/php7.4
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/
RUN install-php-extensions amqp
RUN apt-get update \
    && apt-get install wget gnupg2 -y \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && sh -c "echo 'deb http://dl.google.com/linux/chrome/deb/ stable main' >>   /etc/apt/sources.list" \
    && apt-get update && apt-get install google-chrome-stable=87.* -y