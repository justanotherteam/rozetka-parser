<?php

declare(strict_types=1);

namespace App\Tests\KernelRequest;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\HttpFoundation\JsonResponse;

final class Browser extends KernelBrowser
{
    public function sendRequest(KernelBrowserRequest $request): JsonResponse
    {
        $this->request(
            $request->getMethod(),
            $request->getUri(),
            $request->getParameters(),
            $request->getFiles(),
            $request->getServer(),
            $request->getContent()
        );

        $response = $this->getResponse();

        return new JsonResponse(
            $response->getContent(),
            $response->getStatusCode(),
            $response->headers->all()
        );
    }
}
