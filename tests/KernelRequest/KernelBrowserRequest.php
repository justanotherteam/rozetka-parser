<?php

declare(strict_types=1);

namespace App\Tests\KernelRequest;

use Symfony\Component\BrowserKit\Request;

final class KernelBrowserRequest extends Request
{
    public static function json(string $method, string $uri, array $content = null): self
    {
        return new self(
            $uri,
            $method,
            [],
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($content)
        );
    }
}
