<?php

declare(strict_types=1);

namespace App\Tests\Traits;

use App\DataFixtures\UserFixtures;
use App\Tests\KernelRequest\Browser;

trait SetupJWTAuthenticatedClientTrait
{
    use JWTAuthenticationTrait;

    /** @var Browser */
    protected $client;

    abstract protected static function getUsername(): string;

    private static function getUserPassword(): string
    {
        if (UserFixtures::ADMIN_USERNAME === static::getUsername()) {
            return '123';
        }

        return '123';
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->client = static::createClient();
        $this->authenticateClient(
            $this->client,
            static::getUsername(),
            static::getUserPassword()
        );
    }
}
