<?php

declare(strict_types=1);

namespace App\Tests\Traits;

use App\Tests\KernelRequest\Browser;
use App\Tests\KernelRequest\KernelBrowserRequest;
use Symfony\Component\HttpFoundation\Request;

trait JWTAuthenticationTrait
{
    private function authenticateClient(Browser $client, string $username, string $password): void
    {
        static $token;

        if (null === $token) {
            $response = $client->sendRequest(KernelBrowserRequest::json(Request::METHOD_POST, '/api/authentication_token', [
                'username' => $username,
                'password' => $password,
            ]));
            $this->assertResponseIsSuccessful();
            $data = json_decode($response->getContent(), true);
            $this->assertArrayHasKey('token', $data);
            $token = $data['token'];
        }

        $client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $token));
    }
}
